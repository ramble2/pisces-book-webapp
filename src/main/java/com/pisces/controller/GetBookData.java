package com.pisces.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pisces.model.Book;
import com.pisces.service.GetBookDataService;

@WebServlet("/GetBookData")
public class GetBookData extends HttpServlet {

	 private static final long serialVersionUID = 1L;
	 
	   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	     
		  GetBookDataService getBookDataService = new GetBookDataService();
		  List<Book> books = getBookDataService.getListOfBooks();
	 
	      request.setAttribute("books", books);
	 
	      request.getRequestDispatcher("bookDetails.jsp").forward(request, response);        
	    }
}
