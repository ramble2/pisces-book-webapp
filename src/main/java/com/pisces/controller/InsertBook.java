package com.pisces.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import com.pisces.model.Book;
import com.pisces.service.InsertBookService;

@WebServlet("/InsertBook")
public class InsertBook extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	     
        String isbn = request.getParameter("isbn");
        String name = request.getParameter("name");
        String author = request.getParameter("author");
        String publisher = request.getParameter("publisher");
        String price = request.getParameter("price");
 
        Book book = new Book();
        book.setAuthor(author);
        book.setIsbn(isbn);
        book.setName(name);
        book.setPrice(Integer.parseInt(price));
        book.setPublisher(publisher);
        
        InsertBookService insertBookService = new InsertBookService();
        insertBookService.insert(book);
 
        response.sendRedirect("GetBookData");
    }
}