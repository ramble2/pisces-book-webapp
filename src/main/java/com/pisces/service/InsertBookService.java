package com.pisces.service;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.pisces.hibernate.util.HibernateUtil;
import com.pisces.model.Book;

public class InsertBookService {

	public boolean insert(Book book){
	     Session session = HibernateUtil.openSession();
	     if(doesBookExists(book)) return false;  
	     
	     Transaction tx = null;
	     try {
	         tx = session.getTransaction();
	         tx.begin();
	         session.saveOrUpdate(book);       
	         tx.commit();
	     } catch (Exception e) {
	         if (tx != null) {
	             tx.rollback();
	         }
	         e.printStackTrace();
	     } finally {
	         session.close();
	     } 
	     return true;
	}
	
	public boolean doesBookExists(Book book){
	     Session session = HibernateUtil.openSession();
	     boolean result = false;
	     Transaction tx = null;
	     try{
	         tx = session.getTransaction();
	         tx.begin();
	         Query query = session.createQuery("from Book where name='"+book.getName()+"'");
	         Book b = (Book)query.uniqueResult();
	         tx.commit();
	         if(b!=null) result = true;
	     }catch(Exception ex){
	         if(tx!=null){
	             tx.rollback();
	         }
	     }finally{
	         session.close();
	     }
	     return result;
	}
}
