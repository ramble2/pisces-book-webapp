<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Manage Books</title>
     <SCRIPT language=Javascript>
       
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
      
    </SCRIPT>
  </head>
  <body>
     <%@ page import="java.util.List,com.pisces.model.*" %>
     <h3> Insert Book </h3>
     <h4> Enter Books Details  </h4>
     <form action="InsertBook" method="POST">
        <table >
           <tr> <td>ISBN </td> 
              <td> <input type="text" onkeypress="return isNumberKey(event)"  name="isbn" /><span style="color:red;"> *Number only </span></td>
           </tr>
           <tr> <td>NAME </td> 
             <td> <input type="text" name="name" /> </td>
           </tr>
           <tr> <td>AUTHOR </td> 
              <td> <input type="text" name="author" /> </td>
           </tr>
           <tr> <td>PUBLISHER </td> 
              <td> <input type="text" name="publisher" /> </td>
           </tr>
           <tr> <td>PRICE </td> 
              <td> <input type="text" name="price" onkeypress="return isNumberKey(event)" /><span style="color:red;"> *Number only</span> </td>
           </tr>
           <tr> <td><input type="Submit" value="Save"/> </td> 
           </tr>
        </table>
     </form>
   </body>
</html>